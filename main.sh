# argument
## number of available CPU cores
CORES=$1
# run pipeline
## prepare the data : demultiplexing & clone filtering
snakemake -s 00-scripts/step1.sf -j $CORES
## index genome(s)
### check if genome is already indexed
check_already_indexed=`ls 08-genomes/*.ann`
if [[ -z $check_already_indexed ]]; then
    snakemake -s 00-scripts/step2.sf -j $CORES
fi
## assign (run, pool, barcode) to individual according to _infos.csv file and align them onto the genome
snakemake -s 00-scripts/step3.sf -j $CORES -F
## run STACKS2 programs : gstacks & populations
snakemake -s 00-scripts/step4.sf -j $CORES
