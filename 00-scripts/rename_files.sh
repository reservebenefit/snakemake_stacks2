## rename files
#argument 1 : path of the results 04-all_samples/species
#argument 2 : path of the infos files 01-info_files/species_infos.csv
mkdir $1
awk -F, '{ print $1"\t"$2"\t"$3"\t"$4}' $2 | while read NUMB RUN BARCODE NAME_IND ;
do
 FOLDER_RUN=`ls $(pwd)/03-samples/$NUMB/ | grep $RUN | grep clone`
 FILE_R1=`ls $(pwd)/03-samples/$NUMB/$FOLDER_RUN/ | grep $BARCODE | grep ".1.1."`
 FILE_R2=`ls $(pwd)/03-samples/$NUMB/$FOLDER_RUN/ | grep $BARCODE | grep ".2.2."`
 if [ -z "$FILE_R1" ]
 then
  echo "\$var is empty"
 else
  PATH_R1=$(pwd)/03-samples/$NUMB/$FOLDER_RUN/$FILE_R1
  PATH_R2=$(pwd)/03-samples/$NUMB/$FOLDER_RUN/$FILE_R2
  echo $PATH_R1 $PATH_R2
  ln -s $PATH_R1 $1/$NAME_IND".1.fq.gz"
  ln -s $PATH_R2 $1/$NAME_IND".2.fq.gz"
 fi
done
