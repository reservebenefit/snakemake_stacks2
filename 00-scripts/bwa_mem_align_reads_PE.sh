#!/bin/bash

# Global variables
GENOMEFOLDER="08-genomes"
##prefix of the genome files
GENOME=$1
##path of the folder which contains demultiplexed/"clonefiltered" samples
DATAFOLDER=$2
## number of available CPU cores (default : 64)
NCPU=$3


# Test if user specified a number of CPUs
if [[ -z "$NCPU" ]]
then
    NCPU=64
fi
# temporary results from bwa mem command
temp_bwa=$$.temp_bwa

## Index genome if not alread done
#bwa index -p $GENOMEFOLDER/$GENOME $GENOMEFOLDER/$GENOME.fasta

for file in $(ls -1 $DATAFOLDER/*.1.fq.gz)
do
    # Name of uncompressed file
    file2=$(echo "$file" | sed 's/\.1\./.2./g')
    echo "Aligning file $file $file2" 
    name=$(basename $file)
    name2=$(basename $file2)
    ID="@RG\tID:ind\tSM:ind\tPL:Illumina"

    # Align reads 1 step
    bwa mem -t "$NCPU" -R "$ID" $GENOMEFOLDER/$GENOME $DATAFOLDER/"$name" $DATAFOLDER/"$name2" 2> /dev/null > $temp_bwa
    samtools view -Sb -q 20 $temp_bwa > $DATAFOLDER/"${name%.1.fq.gz}".bam
    #samtools view -Sb -q 20 -f 83 -f 163 -f 99 -f 147 - > $DATAFOLDER/"${name%.fq.gz}".bam
    # Sort and index
    samtools sort $DATAFOLDER/"${name%.1.fq.gz}".bam  -o $DATAFOLDER/"${name%.1.fq.gz}".sorted.bam
    samtools index $DATAFOLDER/"${name%.1.fq.gz}".sorted.bam
    rm $temp_bwa
done
